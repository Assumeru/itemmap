'use strict';

const process = require('process');
const fs = require('fs');
const path = require('path');

const OBJECTS = ['Activator', 'Alchemy', 'Apparatus', 'Armor', 'Bodypart', 'Book', 'Clothing',
	'Container', 'Creature', 'Door', 'Ingredient', 'LevelledCreature', 'LevelledItem',
	'Light', 'Lockpick', 'MiscItem', 'Npc', 'Probe', 'RepairTool', 'Static', 'Weapon'];
const PERCENT = 100;
const CELL = 8192;

const resolvers = {};
const masters = [];
const cells = {
	interiors: {},
	exteriors: {}
};
const types = {};

function getFiles() {
	const [,, ...files] = process.argv;
	if(files.length > 1) {
		return files;
	}
	throw new Error('Usage: node generator.js Morrowind.json Tribunal.json Bloodmoon.json MyFile1.json ... MyFileN.json output.json');
}

function resolve(id, cb, count = 1, stack = []) {
	const resolver = resolvers[id];
	if(resolver) {
		if(stack.includes(id)) {
			return console.log(`Detected loop: ${stack.join(' -> ')}`);
		}
		resolver(cb, count, stack.concat(id));
	} else {
		console.log('Missing', id);
	}
}

function handleLevelled(record, key, recordId) {
	const items = record[key] ?? [];
	const chanceNone = (record.chance_none || 0) / PERCENT;
	const chance = (1 - chanceNone) / items.length;
	resolvers[recordId] = (cb, count, stack) => {
		cb(recordId, count);
		items.forEach(entry => {
			const id = entry[0].toLowerCase();
			resolve(id, cb, chance * count, stack);
		});
	};
}

function handleInventory(record, recordId) {
	const inventory = record.inventory ?? [];
	resolvers[recordId] = (cb, count, stack) => {
		cb(recordId, count);
		if(record.type === 'Npc' && record.race) {
			cb(record.race.toLowerCase(), count);
		}
		inventory.forEach(([c, i]) => {
			const id = i.toLowerCase();
			resolve(id, cb, Math.abs(c) * count, stack);
		});
	};
}

function getFileName(base) {
	const index = base.indexOf('.');
	if(index >= 0) {
		return base.slice(0, index);
	}
	return base;
}

function getMasterIndex(name) {
	const lower = name.toLowerCase();
	let index = masters.indexOf(lower);
	if(index < 0) {
		index = masters.length;
		masters.push(lower);
	}
	return index;
}

function parseHeader(record) {
	if(record.type !== 'Header') {
		throw new Error('Expected header, got ' + record.type);
	}
	return record.masters.map(([name]) => {
		const base = getFileName(name);
		return getMasterIndex(base);
	});
}

function getExterior(x, y) {
	const key = `${x},${y}`;
	if(!cells.exteriors[key]) {
		cells.exteriors[key] = { grid: [x, y], refs: [], interiors: [] };
	}
	return cells.exteriors[key];
}

function parseInput(files) {
	for(const file of files) {
		const records = JSON.parse(fs.readFileSync(file, 'utf-8'));
		const currentFileName = getFileName(path.basename(file));
		const currentIndex = getMasterIndex(currentFileName);
		const currentMasters = parseHeader(records[0]);
		records.forEach(record => {
			const recordId = record.id?.toLowerCase();
			if(OBJECTS.includes(record.type)) {
				types[recordId] = record.type;
				if(record.type === 'LevelledItem') {
					handleLevelled(record, 'items', recordId);
				} else if(record.type === 'LevelledCreature') {
					handleLevelled(record, 'creatures', recordId);
				} else if(['Container', 'Creature', 'Npc'].includes(record.type)) {
					handleInventory(record, recordId);
				} else {
					resolvers[recordId] = (cb, count) => {
						cb(recordId, count);
					};
				}
			} else if(record.type === 'Cell') {
				let refs;
				if(record.data.flags & 1) {
					if(!cells.interiors[recordId]) {
						cells.interiors[recordId] = { id: recordId, refs: [] };
					}
					refs = cells.interiors[recordId].refs;
				} else {
					refs = getExterior(...record.data.grid).refs;
				}
				record.references?.forEach(reference => {
					const index = reference.refr_index;
					let master = currentIndex;
					if(reference.mast_index > 0) {
						master = currentMasters[reference.mast_index - 1];
						const find = ref => ref.master === master && ref.index === index;
						let existing = refs.find(find);
						if(!existing) {
							Object.values(cells.exteriors).some(exterior => {
								existing = exterior.refs.find(find);
								return existing;
							});
							if(!existing) {
								Object.values(cells.interiors).some(interior => {
									existing = interior.refs.find(find);
									return existing;
								});
							}
						}
						if(existing) {
							existing.reference = reference;
							return;
						}
					}
					refs.push({ index, master, reference });
				});
			} else if(record.type === 'Race') {
				types[recordId] = record.type;
			}
		});
	}
}

function findExteriors() {
	const found = {};
	Object.values(cells.exteriors).forEach(exterior => {
		exterior.refs.forEach(({ reference }) => {
			if(!('deleted' in reference) && reference.door_destination_cell) {
				const int = reference.door_destination_cell.toLowerCase();
				found[int] = exterior;
			}
		});
	});
	const find = interior => {
		const linkedInts = new Set();
		const foundExt = interior.refs.some(({ reference }) => {
			if(!('deleted' in reference) && reference.door_destination_coords) {
				if(reference.door_destination_cell) {
					const linkedInt = reference.door_destination_cell.toLowerCase();
					if(found[linkedInt]) {
						found[interior.id] = found[linkedInt];
						return true;
					}
					linkedInts.add(linkedInt);
				} else {
					const [xPos, yPos] = reference.door_destination_coords;
					const x = Math.floor(xPos / CELL);
					const y = Math.floor(yPos / CELL);
					found[interior.id] = getExterior(x, y);
					return true;
				}
			}
			return false;
		});
		return [foundExt, linkedInts];
	};
	Object.values(cells.interiors).forEach(interior => {
		const res = find(interior);
		let foundExt = res[0];
		const linkedInts = res[1];
		if(!foundExt) {
			const stack = [...linkedInts];
			for(let i = 0; i < stack.length && !foundExt; i++) {
				const [f, links] = find(cells.interiors[stack[i]]);
				links.forEach(id => {
					if(!linkedInts.has(id)) {
						linkedInts.add(id);
						stack.push(id);
					}
				});
				foundExt = f;
				if(foundExt) {
					found[interior.id] = found[stack[i]];
				}
			}
		}
		if(foundExt) {
			linkedInts.forEach(id => {
				found[id] = found[interior.id];
			});
		}
	});
	for(const id in found) {
		found[id].interiors.push(cells.interiors[id]);
	}
}

function resolveItems() {
	const resolveCell = (cell, cb) => {
		cell.refs.forEach(({ reference }) => {
			if(!('deleted' in reference)) {
				const id = reference.id.toLowerCase();
				resolve(id, cb);
			}
		});
	};
	const out = [];
	const ids = new Set();
	Object.values(cells.exteriors).forEach(exterior => {
		const [x, y] = exterior.grid;
		const cell = { x, y, items: {} };
		const cb = (id, count) => {
			if(!(id in cell.items)) {
				cell.items[id] = 0;
				ids.add(id);
			}
			cell.items[id] += count;
		};
		resolveCell(exterior, cb);
		exterior.interiors.forEach(interior => {
			resolveCell(interior, cb);
		});
		if(Object.keys(cell.items).length) {
			out.push(cell);
		}
	});
	const byType = {};
	ids.forEach(id => {
		const type = types[id];
		if(!byType[type]) {
			byType[type] = [];
		}
		byType[type].push(id);
	});
	for(const type in byType) {
		byType[type].sort();
	}
	return {
		cells: out,
		ids: byType
	};
}

const files = getFiles();
const outputFile = files.pop();
parseInput(files);
findExteriors();
const cellData = resolveItems();
fs.writeFileSync(outputFile, JSON.stringify(cellData));
