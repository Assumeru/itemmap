import React, { useState, memo } from 'react';

function ItemList({ search, items, selected, setSelected }) {
	let all = true;
	const filtered = search ? [] : items;
	const labels = items.map(item => {
		const shown = !search || item.includes(search);
		if(shown && search) {
			filtered.push(item);
		}
		if(!selected[item] && shown) {
			all = false;
		}
		return <label key={item} hidden={!shown || undefined}>
			<input type="checkbox" checked={selected[item] ?? false} onChange={e => setSelected(prev => ({ ...prev, [item]: e.target.checked }))} /> {item}
		</label>;
	});
	return <>
		<label>
			<input type="checkbox" checked={all} onChange={e => {
				if(e.target.checked) {
					setSelected(prev => {
						const copy = { ...prev };
						filtered.forEach(item => {
							copy[item] = true;
						});
						return copy;
					});
				} else {
					setSelected(search ? prev => {
						const copy = { ...prev };
						filtered.forEach(item => {
							copy[item] = false;
						});
						return copy;
					} : {});
				}
			}} />
		</label>
		{labels}
	</>;
}

function Sidebar({ items, selected, setSelected }) {
	const [search, setSearch] = useState('');
	const [type, setType] = useState(null);
	return <div id="sidebar">
		{Object.keys(items).map(t => <button key={t} onClick={e => {
			e.preventDefault();
			setType(t);
		}} className={type === t ? 'active' : undefined}>{t}</button>)}
		<input type="search" value={search} onChange={e => setSearch(e.target.value.toLowerCase())} />
		{items[type] && <ItemList search={search} items={items[type]} selected={selected} setSelected={setSelected} />}
	</div>;
}

export default memo(Sidebar);
