import React, { memo } from 'react';

function TableRows({ items }) {
	const targetLength = Object.values(items).reduce((o, c) => {
		const [whole] = c.toString().split('.');
		return Math.max(o, whole.length);
	}, 0);
	return Object.keys(items).sort().map(item => {
		let count = items[item].toString();
		const [whole] = count.split('.');
		for(let i = targetLength - whole.length; i > 0; i--) {
			count = `\u2007${count}`;
		}
		return <tr key={item}>
			<td>{item}</td>
			<td>{count}</td>
		</tr>;
	});
}

function CellView({ cell }) {
	return <div id="cell-view">
		{cell && <>
			<h4>{cell.x}, {cell.y}</h4>
			<table>
				<thead>
					<tr>
						<th>Item</th>
						<th>Count</th>
					</tr>
				</thead>
				<tbody>
					<TableRows items={cell.items} />
				</tbody>
			</table>
		</>}
	</div>;
}

export default memo(CellView);
