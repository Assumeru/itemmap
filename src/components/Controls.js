import React, { memo } from 'react';

function downloadSVG(svg) {
	const blob = new Blob([svg.outerHTML], { type: 'image/svg+xml;charset=utf-8' });
	const url = URL.createObjectURL(blob);
	const a = document.createElement('a');
	a.href = url;
	a.download = 'map.svg';
	document.body.append(a);
	a.click();
	document.body.removeChild(a);
}

function Controls({ zoom, setZoom, svgRef, minZoom, maxZoom }) {
	return <div id="controls">
		<button onClick={e => {
			e.preventDefault();
			downloadSVG(svgRef.current);
		}}>Save</button> <input type="range" min={minZoom} max={maxZoom} value={zoom} onChange={e => setZoom(+e.target.value)} />
	</div>;
}

export default memo(Controls);
