import React, { memo, useMemo, forwardRef } from 'react';
import { MAP_URL, IMAGE_WIDTH, IMAGE_HEIGHT, CELL_WIDTH, CELL_HEIGHT, ORIGIN_X, ORIGIN_Y } from './mapdata';
const COLOR_MAX = 255;
const STYLE = `rect {
	opacity: 0.9;
}`;

function getCellTotal(items, selected) {
	if(!items) {
		return 0;
	}
	let total = 0;
	for(const item in selected) {
		if(selected[item] && item in items) {
			total += items[item];
		}
	}
	return total;
}

function getFill(total, max) {
	const slices = 5;
	const percentage = total === 0 ? 0 : total / max;
	let red = 0;
	let green = 0;
	let blue = 0;
	/* eslint-disable no-magic-numbers */
	if(percentage >= 4 / slices) {
		red = COLOR_MAX;
		green = Math.round((1 - (percentage - 4 / slices) * slices) * COLOR_MAX);
	} else if(percentage >= 3 / slices) {
		red = Math.round((percentage - 3 / slices) * slices * COLOR_MAX);
		green = COLOR_MAX;
	} else if(percentage >= 2 / slices) {
		green = COLOR_MAX;
		blue = Math.round((1 - (percentage - 2 / slices) * slices) * COLOR_MAX);
	} else if(percentage >= 1 / slices) {
		blue = COLOR_MAX;
		green = Math.round((percentage - 1 / slices) * slices * COLOR_MAX);
	} else {
		blue = Math.round(percentage * slices * COLOR_MAX);
	}
	/* eslint-enable no-magic-numbers */
	return `rgb(${red}, ${green}, ${blue})`;
}

const SVGMap = memo(forwardRef(({ cells, selected, maxX, minX, maxY, minY, setCell, selectedCell, total, width, height }, svgRef) => {
	const viewBox = [minX * CELL_WIDTH, -maxY * CELL_HEIGHT, width * CELL_WIDTH, height * CELL_HEIGHT];
	const grid = [];
	for(let x = minX; x <= maxX; x++) {
		for(let y = minY; y <= maxY; y++) {
			const key = `${x},${y}`;
			const cellTotal = getCellTotal(cells[key]?.items, selected);
			const fill = getFill(cellTotal, total);
			grid.push(<rect
				width={CELL_WIDTH}
				height={CELL_HEIGHT}
				x={x * CELL_WIDTH}
				y={-y * CELL_WIDTH}
				key={key}
				fill={fill}
				onClick={() => setCell(cells[key])}
				className={selectedCell === cells[key] ? 'active' : undefined} />);
		}
	}
	return <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox={viewBox.join(' ')} height={height * CELL_HEIGHT} width={width * CELL_WIDTH} preserveAspectRatio="xMinYMin meet" id="map" ref={svgRef}>
		<style>{STYLE}</style>
		<image x={-ORIGIN_X} y={CELL_HEIGHT - ORIGIN_Y} width={IMAGE_WIDTH} height={IMAGE_HEIGHT} xlinkHref={MAP_URL} />
		{grid}
	</svg>;
}));

function Map({ minX, maxX, minY, maxY, cells, selected, cell: selectedCell, setCell, zoom, width, height }, svgRef) {
	const total = useMemo(() => {
		let max = 0;
		Object.values(cells).forEach(cell => {
			let sum = 0;
			for(const item in selected) {
				if(selected[item] && cell.items[item]) {
					sum += cell.items[item];
				}
			}
			max = Math.max(sum, max);
		});
		return max;
	}, [selected, cells]);
	return <>
		<div style={{ width: `${zoom}px` }}>
			<SVGMap cells={cells} selected={selected} maxX={maxX} minX={minX} maxY={maxY} minY={minY} setCell={setCell} selectedCell={selectedCell} total={total} width={width} height={height} ref={svgRef} />
		</div>
		<div id="map-max">Max count: {total}</div>
	</>;
}

export default memo(forwardRef(Map));
