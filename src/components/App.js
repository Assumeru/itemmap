import React, { useState, useRef } from 'react';
import Map from './Map';
import Sidebar from './Sidebar';
import CellView from './CellView';
import Controls from './Controls';
import { CELL_WIDTH } from './mapdata';
import DATA from '../data/cells.json';
const DEFAULT_ZOOM = 1000;

function parseCellData() {
	const items = {};
	const x = { min: 0, max: 0 };
	const y = { min: 0, max: 0 };
	const cells = {};
	DATA.cells.forEach(cell => {
		Object.assign(items, cell.items);
		x.min = Math.min(x.min, cell.x);
		x.max = Math.max(x.max, cell.x);
		y.min = Math.min(y.min, cell.y);
		y.max = Math.max(y.max, cell.y);
		cells[`${cell.x},${cell.y}`] = cell;
	});
	const width = x.max - x.min + 1;
	const height = y.max - y.min + 1;
	return {
		x, y, width, height,
		items: DATA.ids,
		cells
	};
}

const { x, y, items, cells, width, height } = parseCellData();

export default function App() {
	const svgRef = useRef();
	const [selected, setSelected] = useState({});
	const [cell, setCell] = useState(null);
	const [zoom, setZoom] = useState(DEFAULT_ZOOM);
	return <>
		<div id="map-wrapper">
			<Map minX={x.min} maxX={x.max} minY={y.min} maxY={y.max} cells={cells} selected={selected} cell={cell} setCell={setCell} zoom={zoom} width={width} height={height} ref={svgRef} />
		</div>
		<div id="sidebar-wrapper">
			<Controls zoom={zoom} setZoom={setZoom} svgRef={svgRef} minZoom={DEFAULT_ZOOM} maxZoom={width * CELL_WIDTH} />
			<CellView cell={cell} />
			<Sidebar items={items} selected={selected} setSelected={setSelected} />
		</div>
	</>;
}
