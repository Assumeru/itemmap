export const MAP_URL = './map.png';
export const IMAGE_WIDTH = 4121;
export const IMAGE_HEIGHT = 4081;
export const CELL_WIDTH = 40;
export const CELL_HEIGHT = CELL_WIDTH;
export const ORIGIN_X = 1680;
export const ORIGIN_Y = 1520;
